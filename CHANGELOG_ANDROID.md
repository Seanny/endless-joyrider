# Changelog #

## Version 1.2 ##
- Game is now compaitable with devices running Android 4.0 (Ice Cream Sandwitch) or Newer
- 

## Version 1.1.1 ##
- Quick hotfix to fix Leaderboard as end of game

## Version 1.1 ##
    Added Leaderboard and login support for Google Play Games Service.
    Performance Improvements.
    Slight UI Scale Improvement.
    Updated splash screen on startup.
    
## Version 1.0 ##
    Initial Release
