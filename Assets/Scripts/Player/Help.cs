﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Help : MonoBehaviour {

	public Text helpText;
	public Text scoreText;

	public Button leftButton;
	public Button rightButton;

	private int highScore;
	private int currentScore;

	private string helpString;

	// Use this for initialization
	void Start () {
		#if (UNITY_IPHONE || UNITY_ANDROID)
			helpString = "Tap the SCREEN to move forward!\nUse the <b>LEFT ARROW</b> to move left\nUse the <b>RIGHT ARROW</b> to move right";
		#elif UNITY_TVOS
			helpString = "Tap the REMOTE to move forward!"; // tvOS Player
		#elif UNITY_WSA
			helpString = "Tap the SCREEN or SPACE to move forward!\nUse the <b>LEFT ARROW</b> to move left\nUse the <b>RIGHT ARROW</b> to move right"; // Windows Store Players
		#else
			helpString = "Click LEFT MOUSE to move forward!\nUse <b>A</b> or <b>LEFT ARROW</b> to move left\nUse <b>D</b> or <b>RIGHT ARROW</b> to move right"; // Desktop platforms such as Desktop Windows, macOS and Linux
			leftButton.gameObject.SetActive(false);
			rightButton.gameObject.SetActive(false);
			Debug.Log(GetJoystickNames());
		#endif
		helpString = helpString + "\n\nAvoid the Barriers and Cones whilst also trying to collect the coins!";
		helpText.text = helpString;
	}
	
	// Update is called once per frame
	void Update () {
		highScore = PlayerPrefs.GetInt("score");
		currentScore = PlayerPrefs.GetInt("currentScore");
		//currentLives = PlayerPrefs.GetInt("currentLives");
		scoreText.text = "Score: " + currentScore + " | High Score: " + highScore;
		//Score: 99999 | High Score: 99999

	}
}