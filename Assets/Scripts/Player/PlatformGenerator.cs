﻿using UnityEngine;
using System.Collections;

public class PlatformGenerator : MonoBehaviour 
{
	public GameObject thePlatform;
	public GameObject coin;
	public GameObject barrier;
	public Transform generationPoint;

	// Use this for initialization
	void Start () 
	{
		thePlatform = GameObject.Find ("Ground_Medium");
		coin = GameObject.Find ("Coin");
		barrier = GameObject.Find ("Barrier");
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (transform.position.x < generationPoint.position.x) 
		{
			//Debug.Log("Generating new platform: " + thePlatform);
			transform.position = new Vector3 (transform.position.x + 14f, 0, 0);
			Instantiate (thePlatform, transform.position, Quaternion.identity);

			//Debug.Log("Generating new coin " + coin);
			int randVal = Random.Range(1, 3);
			//Debug.Log(randVal.ToString());
			if(randVal == 1)
			{
				transform.position = new Vector3 (transform.position.x, transform.position.y + 1.45f, 1);
			}
			else
			{
				transform.position = new Vector3 (transform.position.x, transform.position.y + 1.45f, -1);
			}
			transform.rotation = Quaternion.Euler(0, 0, 90);
			Instantiate (coin, transform.position, transform.rotation);

			randVal = Random.Range(1, 3);
			//Debug.Log(randVal.ToString());
			if(randVal == 1)
			{
				transform.position = new Vector3 (transform.position.x + 2, 0.6f, 1);
			}
			else
			{
				transform.position = new Vector3 (transform.position.x + 2, 0.6f, -1);
			}
			transform.rotation = Quaternion.Euler(270, 0, 90);
			Instantiate (barrier, transform.position, transform.rotation);
		}
	}
}