﻿using UnityEngine;
using System.Collections;

public class PlatformDestruction : MonoBehaviour {

	public GameObject platformDescructionPoint;

	// Use this for initialization
	void Start () 
	{
		platformDescructionPoint = GameObject.Find ("PlatformDestructionPoint");
	}
	
	// Update is called once per frame
	void Update() 
	{
		object[] obj = GameObject.FindGameObjectsWithTag("Ground");
		foreach (object o in obj)
		{
			GameObject g = (GameObject) o;
			var objectPosition = g.transform.position.x;

			if (objectPosition < platformDescructionPoint.transform.position.x)
			{
				Destroy (g);
			}
		}

		obj = GameObject.FindGameObjectsWithTag("Coin");
		foreach (object o in obj)
		{
			GameObject g = (GameObject) o;
			var objectPosition = g.transform.position.x;

			if (objectPosition < platformDescructionPoint.transform.position.x)
			{
				Destroy (g);
			}
		}

		obj = GameObject.FindGameObjectsWithTag("Barrier");
		foreach (object o in obj)
		{
			GameObject g = (GameObject) o;
			var objectPosition = g.transform.position.x;

			if (objectPosition < platformDescructionPoint.transform.position.x)
			{
				Destroy (g);
			}
		}
	}
}