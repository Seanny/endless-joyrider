﻿using UnityEngine;
using System.Collections;

public class CoinGenerator : MonoBehaviour {

	public GameObject coin;
	public Transform generationPoint;

	// Use this for initialization
	void Start () 
	{
		coin = GameObject.Find ("Coin");
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (transform.position.x < generationPoint.position.x) 
		{
			Debug.Log(transform.position);
			int randVal = Random.Range(0, 1);
			if(randVal == 1)
			{
				transform.position = new Vector3 (transform.position.x + 16, 1.5f, 1);
			}
			else
			{
				transform.position = new Vector3 (transform.position.x + 16, 1.5f, -1);
			}
			transform.rotation = Quaternion.Euler(0, 0, 90);
			Instantiate (coin, transform.position, transform.rotation);
		}
	}
}
