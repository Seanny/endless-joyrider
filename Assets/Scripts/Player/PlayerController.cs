﻿// Unity
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Analytics;
using UnityEngine.SocialPlatforms;

// MSCorelib
using System.Collections.Generic;
using System.Collections;

// Android
using EndlessJoyrider.Android;

[RequireComponent(typeof(AudioSource))]
public class PlayerController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler 
{
	//Audio
	public AudioClip coinCollect;
    private AudioSource audio;

	// Vehicle Info
	public float vehicleSpeed = 5;
	public Rigidbody vehicle;

	// UI Text
	public GameObject startText;

	// Global Booleans
	public bool gameStarted;
	
	// Score System
	public int scoreValue = 0;
	public int highScore;
	
	// Touch UI
	public bool leftButtonHolding;
	public bool rightButtonHolding;

	public void OnPointerUp(PointerEventData ped) 
	{ 

	}

	public void OnPointerDown(PointerEventData ped) 
	{ 
		
	}
	
	public void OnLeftDown()
	{
		leftButtonHolding = true;
	}

	public void OnLeftUp()
	{
		leftButtonHolding = false;
	}

	public void OnRightDown()
	{
		rightButtonHolding = true;
	}

	public void OnRightUp()
	{
		rightButtonHolding = false;
	}

	void GoLeft()
	{
		transform.Translate(0, 0, 5 * Time.deltaTime);
	}

	public void ReloadLevel(string reason)
	{
		if(SocialStatus.UserLoggedIn == true)
		{
			Analytics.CustomEvent("GameOver", new Dictionary<string, object>
			{
				{ "SessionScore", scoreValue },
				{ "HighScore", highScore },
				{ "Reason", reason }
			});

			Social.ReportScore (highScore, "CgkIhcOu9JwCEAIQBg", success => {
				#if UNITY_EDITOR
				Debug.Log(success ? "Reported score successfully" : "Failed to report score");
				#endif
			});
		}

		PlayerPrefs.SetInt("score", highScore);
		PlayerPrefs.Save ();
		
		SceneManager.LoadScene("GameOverHandler");
	}

	// Use this for initialization
	void Start () 
	{
		vehicle = GetComponent<Rigidbody>();
		vehicleSpeed = 5;
		scoreValue = 0;
		highScore = PlayerPrefs.GetInt("score");
		gameStarted = false;
		audio = GetComponent<AudioSource>();

		Application.targetFrameRate = 30;
		
	}
	
	void OnCollisionEnter(Collision collision) 
	{
        if(collision.gameObject.CompareTag("Barrier"))
		{
			ReloadLevel("barrier");
		}
    }
	
	void OnTriggerEnter(Collider other) 
	{
		/*
		 * Detect if the player collides with a coin
		 */
		if(other.gameObject.CompareTag("Coin"))
		{
			/*
			 * Set the players score, update highscore and destroy coin.
			 */
			//PlayerPrefs.SetInt("currentScore", scoreValue);
			audio.PlayOneShot(coinCollect, 1.0f);
			scoreValue = scoreValue + 1;
			
			if (scoreValue > highScore) 
			{
				highScore = scoreValue;
				PlayerPrefs.SetInt("score", highScore);
				PlayerPrefs.Save ();
			}
			Destroy(other.gameObject);
		}
	}

	void LateUpdate()
	{
		if(gameStarted == true)
		{
			if(vehicle.velocity == Vector3.zero)
			{
				vehicle.velocity = new Vector2(vehicleSpeed, vehicle.velocity.y + 1);
			}
		}
		
	}

	void loadLevel()
	{
		Destroy(startText);
		gameStarted = true;
	}

	// Update is called once per frame
	void Update () 
	{
		PlayerPrefs.SetInt("currentScore", scoreValue);
		if(gameStarted == true)
		{
			vehicle.velocity = new Vector2(vehicleSpeed, vehicle.velocity.y + 0.16f);

			//Move the players vehicle left if they swipe left, use A or LeftArrow.
			if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow) || leftButtonHolding == true) 
			{
				transform.Translate(0, 0, 5 * Time.deltaTime);
			}
			
			//Move the players vehicle right if they swipe right, use D or RightArrow.
			if(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow) || rightButtonHolding == true) 
			{
				transform.Translate(0, 0, -5 * Time.deltaTime);
			}
			if(scoreValue == 1 && PlayerPrefs.GetInt("1_Coin") != 1)
			{
				Social.ReportProgress("CgkIhcOu9JwCEAIQAQ", 100.0f, (bool success) => {
					if (success)
						PlayerPrefs.SetInt("1_Coin", 1); Debug.Log("1 Coins Achieved");
				});
			}

			if(scoreValue == 10 && PlayerPrefs.GetInt("10_Coin") != 1)
			{
				Social.ReportProgress("CgkIhcOu9JwCEAIQAg", 100.0f, (bool success) => {
					if (success)
						PlayerPrefs.SetInt("10_Coin", 1); Debug.Log("10 Coins Achieved");
				});
			}

			if(scoreValue == 20 && PlayerPrefs.GetInt("20_Coin") != 1)
			{
				Social.ReportProgress("CgkIhcOu9JwCEAIQAw", 100.0f, (bool success) => {
					if (success)
						PlayerPrefs.SetInt("20_Coin", 1); Debug.Log("20 Coins Achieved");
				});
			}

			if(scoreValue == 50 && PlayerPrefs.GetInt("50_Coin") != 1)
			{
				Social.ReportProgress("CgkIhcOu9JwCEAIQBA", 100.0f, (bool success) => {
					if (success)
						PlayerPrefs.SetInt("50_Coin", 1); Debug.Log("50 Coins Achieved");
				});
			}

			if(scoreValue == 100 && PlayerPrefs.GetInt("100_Coin") != 1)
			{
				Social.ReportProgress("CgkIhcOu9JwCEAIQBQ", 100.0f, (bool success) => {
					if (success)
						PlayerPrefs.SetInt("100_Coin", 1); Debug.Log("100 Coins Achieved");
				});
			}
			
			if(vehicle.position.y < -20f) 
			{
				ReloadLevel("BelowMap");
			}
		}
		else
		{
			if(Input.GetMouseButtonDown(0))
			{
				loadLevel();
			}
		}		
	}
}
