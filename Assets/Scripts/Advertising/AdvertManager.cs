﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
#if UNITY_ADS
using UnityEngine.Advertisements;
using GoogleMobileAds;
using GoogleMobileAds.Api;
#endif

public class AdvertManager : MonoBehaviour 
{
	#if UNITY_ADS
	public static BannerView bannerView;
	public static BannerView scoreBannerView;
	
	void Start()
	{
		#if UNITY_EDITOR
		Debug.Log("[AdvertManager] DontDestroyOnLoad");
		#endif
		DontDestroyOnLoad(gameObject);

		RequestBanner();
	}

	private static void RequestBanner()
	{
		#if UNITY_EDITOR
			string adUnitId = "unused";
		#elif UNITY_ANDROID
			string adUnitId = "ca-app-pub-1119258824927173/1713696844";
		#elif UNITY_IPHONE
			string adUnitId = "ca-app-pub-1119258824927173/8180752445";
		#else
			string adUnitId = "unexpected_platform";
		#endif

		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
		
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder()
		.Build();
		// Load the banner with the request.
		bannerView.LoadAd(request);
	}
	
	#endif
}