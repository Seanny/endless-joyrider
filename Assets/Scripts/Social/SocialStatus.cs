﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocialStatus : MonoBehaviour 
{

	private static bool isLogged;

	[SerializeField]
	public static bool UserLoggedIn
	{
		get 
		{ 
			return isLogged; 
		}
		set
		{
			isLogged = value;
		}
		
	}

	// Use this for initialization
	void Start () 
	{
		isLogged = false;
		DontDestroyOnLoad(gameObject);	
	}
}
