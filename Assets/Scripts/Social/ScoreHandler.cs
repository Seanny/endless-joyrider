﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
#if UNITY_ADS
using UnityEngine.Advertisements;
#endif
using GoogleMobileAds.Api;
using UnityEngine.Analytics;

public class ScoreHandler : MonoBehaviour 
{
	public Text gameOverText;
	public Text currentScoreText;

	private InterstitialAd interstitial;

	private void RequestInterstitial()
	{
		#if UNITY_ANDROID
			string adUnitId = "ca-app-pub-1119258824927173/3748833242";
		#elif UNITY_IPHONE
			string adUnitId = "ca-app-pub-1119258824927173/2134218845";
		#else
			string adUnitId = "unexpected_platform";
		#endif

		// Create an interstitial.
        this.interstitial = new InterstitialAd(adUnitId);

        // Register for ad events.
        /*this.interstitial.OnAdLoaded += this.HandleInterstitialLoaded;
        this.interstitial.OnAdFailedToLoad += this.HandleInterstitialFailedToLoad;
        this.interstitial.OnAdOpening += this.HandleInterstitialOpened;
        this.interstitial.OnAdClosed += this.HandleInterstitialClosed;
        this.interstitial.OnAdLeavingApplication += this.HandleInterstitialLeftApplication;*/

        // Load an interstitial ad.
        this.interstitial.LoadAd(this.CreateAdRequest());
	}

	private AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder()
		.TagForChildDirectedTreatment(true)
		.Build();
    }

	// Use this for initialization
	void Start () 
	{
		RequestInterstitial();
		int AdsHandling = Random.Range(0,2);

		// Detect if running on a UnityAds enabled platform (Android and iOS as of 8 December 2016)
		#if UNITY_ADS
		Debug.Log("[SceneHandler] AdsHandling = " + AdsHandling);

		if(AdsHandling == 0)
		{
			Debug.Log("[SceneHandler] Wants UnityAd");
			if(Advertisement.IsReady())
			{
				Debug.Log("[SceneHandler] Showing UnityAd");
				Advertisement.Show();
				Analytics.CustomEvent("Advertisements", new Dictionary<string, object>
				{
					{ "Handler", "UnityAds" },
					{ "Sucessful", true}
				});
			}
			else 
			{
				Debug.Log("[SceneHandler] UnityAd not ready");
				Analytics.CustomEvent("Advertisements", new Dictionary<string, object>
				{
					{ "Handler", "UnityAds" },
					{ "Sucessful", false}
				});
			}
		}
		else
		{
			Debug.Log("[SceneHandler] Wants AdMob");
			if(interstitial.IsLoaded())
			{
				interstitial.Show();
			}
			else 
			{
				Debug.Log("[SceneHandler] AdMob not loaded");
				Analytics.CustomEvent("Advertisements", new Dictionary<string, object>
				{
					{ "Handler", "AdMob" },
					{ "Sucessful", false}
				});
			}
		}

		#endif

		if(SocialStatus.UserLoggedIn == true || Application.isEditor)
		{
			Social.ShowLeaderboardUI();
			
			/*ILeaderboard lb = PlayGamesPlatform.Instance.CreateLeaderboard();
			lb.id = "MY_LEADERBOARD_ID";
			lb.LoadScores(ok =>
				{
					if (ok) {
						LoadUsersAndDisplay(lb);
					}
					else {
						Debug.Log("Error retrieving leaderboardi");
					}
				});*/
		}
		currentScoreText.text = "<b>My Score:</b> " + PlayerPrefs.GetInt("currentScore") + "\n<b>My High Score:</b> " + PlayerPrefs.GetInt("score");
		gameOverText.gameObject.SetActive(true);
	}

	public void ReturnButtonHandler()
	{
		SceneManager.LoadScene("Scene");
	}
}
