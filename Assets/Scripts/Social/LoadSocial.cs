﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
#if (UNITY_ANDROID || (UNITY_IPHONE && !NO_GPGS))
using GooglePlayGames;
#endif

public class LoadSocial : MonoBehaviour 
{
	public Text loadingText;
	public Scrollbar loadingBar;

	private string socialAPI;

	// Use this for initialization
	private void Start () 
	{
		loadingText.text = "Loading Social Platform, please wait....";

		loadingBar.size = 0.1f;
		SocialStatus.UserLoggedIn = false;		
        #if (UNITY_ANDROID || (UNITY_IPHONE && !NO_GPGS)) // Google Play Games
		GooglePlayGames.PlayGamesPlatform.Activate();
		socialAPI = "Google Play Games";
		#else
			socialAPI = "Game Center";
		#endif
		loadingBar.size = 0.2f;

		loadingText.text = "Attempting to Authenticate User, please wait...";
		loadingBar.size = 0.3f;
		Social.localUser.Authenticate(loginHandler);
	}
	
	private void loadGame()
	{
		loadingBar.size = 0.9f;
		SceneManager.LoadScene("MainMenu");
	}

	private void loginHandler(bool success)
	{
		loadingBar.size = 0.4f;
		if (success) 
		{
			loadingText.text = "User authenticated, transitioning to menu...";
			SocialStatus.UserLoggedIn = true;
			loadingBar.size = 0.5f;
		} 
		else 
		{
			if(SocialStatus.UserLoggedIn == true)
			{	
				SocialStatus.UserLoggedIn = false; // Failsafe, can never be too sure! :)
			}
			loadingBar.size = 0.5f;
			loadingText.text = "Cannot Login to Social Platform.\n\nScores will not be saved during gameplay!";
		}
		loadingBar.size = 0.6f;
		loadGame();
	}
}
