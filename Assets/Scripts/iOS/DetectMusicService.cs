﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices; 

namespace EndlessJoyrider.iOS
{
	public class DetectMusicPlayer
	{
		#if UNITY_IPHONE
		[DllImport("__Internal")]
		static private extern bool _DetectMusicPlayer();
		[DllImport("__Internal")]
		static private extern void _ResumeMusicPlayer();
		#endif

		static public bool Detect()
		{
			#if UNITY_IOS
			return _DetectMusicPlayer();
			#else
				return false;
			#endif
		}

		static public void ResumePlayer()
		{
			#if UNITY_IPHONE
			Debug.Log("Resuming music player");
			_ResumeMusicPlayer();
			#endif
		}

		void OnApplicationFocus(bool isFocused)
		{
			#if UNITY_IPHONE
			bool musicPlaying = Detect();
		
			if (musicPlaying)
			{
				Debug.Log("[DetectMusicPlayer.cs] iTunes Audio playing, mute game Background audio");
			}
			else
			{
				Debug.Log("[DetectMusicPlayer.cs] iTunes Audio not playing, enable music.");
			}
			#endif
		}
	}
}