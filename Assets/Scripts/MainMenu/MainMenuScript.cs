﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour {

	public GameObject removeAdsButton;

	

	void Start()
	{
		if(Application.platform != RuntimePlatform.IPhonePlayer)
		{
			removeAdsButton.SetActive(false);
		}
	}

	

	public void StartGame()
	{
		#if UNITY_ADS
		AdvertManager.bannerView.Hide();
		#endif
		SceneManager.LoadScene("Scene");
	}

	public void ShowLeaderboard()
	{
		Social.ShowLeaderboardUI();
	}
}
