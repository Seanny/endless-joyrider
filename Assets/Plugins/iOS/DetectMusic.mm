 #import <MediaPlayer/MediaPlayer.h>
 
 extern "C" {
 
     bool _DetectMusicPlayer() 
     {
         bool playerDetectedAndPlaying = false;
         NSString *reqSysVer = @"3.0";
         NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
         if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
         {
             Class MusicPlayerController = NSClassFromString(@"MPMusicPlayerController");
             if (MusicPlayerController)
             {
                 id myMusicPlayerController = [[MusicPlayerController alloc]init];
                 id MusicPlayer = [[myMusicPlayerController class] iPodMusicPlayer ];
                 if ( [ MusicPlayer playbackState ] == MPMusicPlaybackStatePlaying ) 
                 {
                     playerDetectedAndPlaying = true;
                 }
             }
         }
 
         return playerDetectedAndPlaying;
     }
     
     void _ResumeMusicPlayer()
     {
         NSString *reqSysVer = @"3.0";
         NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
         if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
         {
             Class MusicPlayerController = NSClassFromString(@"MPMusicPlayerController");
             if (MusicPlayerController)
             {
                 id myMusicPlayerController = [[MusicPlayerController alloc]init];
                 id MusicPlayer = [[myMusicPlayerController class] iPodMusicPlayer ];
                 if ( [ MusicPlayer playbackState ] != MPMusicPlaybackStatePlaying ) 
                 {
                     [myMusicPlayerController play];
                 }
             }
         }
     }
     
 }
